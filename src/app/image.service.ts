import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-46539.appspot.com/o/';
  public images:string[] = [];
  constructor() { 
    this.images[0] = this.path + 'biz.png' + '?alt=media&token=925dcf62-13ed-4c3e-ad12-aaf431c863a2';
    this.images[1] = this.path + 'enterment.jfif' + '?alt=media&token=863f9bdd-fa21-4408-a9dc-7adabbdf7607';
    this.images[2] = this.path + 'politics.png' + '?alt=media&token=951b0b6e-6a1e-4c57-96ac-411dbb5b45a1';
    this.images[3] = this.path + 'sport.jfif' + '?alt=media&token=4fad1426-60b0-4cf5-abb8-8e175c47c76f';
    this.images[4] = this.path + 'tech.png' + '?alt=media&token=82ac5517-5da9-4c82-bb59-bfc32e48d5f6'
  }
}
