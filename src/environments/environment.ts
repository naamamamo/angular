// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCuy779Q5N94eZhGcggc_Tbyw350JHnlVU",
    authDomain: "hello-46539.firebaseapp.com",
    databaseURL: "https://hello-46539.firebaseio.com",
    projectId: "hello-46539",
    storageBucket: "hello-46539.appspot.com",
    messagingSenderId: "416046481960",
    appId: "1:416046481960:web:d3db9fa2675bd852182b1d"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
